// const supabase = require("./lib/supabase");
require('dotenv').config();
const { createClient } = require("@supabase/supabase-js");
const axios = require("axios");
const date = require("date-and-time");

const supabase = createClient(
  process.env.SUPABASE_URL,
  process.env.SUPABASE_KEY
);

const semaphoreApiKey = process.env.SEMAPHORE_APIKEY;

async function getAttendance() {
    const dateNow = new Date();
    const { data, error } = await supabase
      .from("enrolles")
      .select(
        `id, fullname,guardian_name, guardian_contact, section_name, 
        attendance(id,day,month,year,status, scheduler(start_time, end_time, subjects(subject_name, subject_code), year_level(level_code, level_name), teacher(full_name)))`
      )
      .eq("attendance.day", dateNow.getDate()) //day variable
      .eq("attendance.month", dateNow.getMonth() + 1) // month
      .eq("attendance.year", dateNow.getFullYear()) // year
      .order("fullname", { ascending: true });
    
    if (error) {
        console.error(error);
        return;
    }

    return data;
}

async function executeSMS() {
  const d = await getAttendance();
  const f = d.filter((att) => att.attendance.length > 0); // have attendance record only
  let ret = ``;
  let count = 0;

  f.map((student) => {
    ret += `Student Name: ${student.fullname}\n`;
    ret += `----------------\n`;
    ret += `Attendance Summary\n`;
    ret += `================\n`;

    count += 1;
    // get attendance
    student.attendance.map((att) => {
      ret += `Teacher Name: ${att.scheduler.teacher.full_name}\n`;
      ret += `${date.format(new Date(att.scheduler.start_time), "h:MM A")} - `;
      ret += `${date.format(new Date(att.scheduler.end_time), "h:MM A")} | `;
      ret += `${att.scheduler.subjects.subject_name} | `;
      ret += `${att.status === "P" ? "PRESENT" : "ABSENT"}`;
    });

    ret += `\n----------------\n`;
    ret += `End of Summary\n\n`;

    // send msg each
    sendSMS(student.guardian_contact, ret);
    ret = ``;
  });

  if (count === f.length) {
    const { data, error } = await supabase
      .from("sms_schedule")
      .update({
        test: 0,
      })
      .eq("id", 3);

    if (error) {
      console.error(error);
      return;
    }
  }
}

function sendSMS(phoneNumber, message) {
    if (message.length <= 0 || phoneNumber.length <= 0) {
      return;
    }
    axios.post("https://api.semaphore.co/api/v4/messages", {
        apikey: semaphoreApiKey,
        number: phoneNumber,
        message: message
      })
      .then(function (response) {
        console.log(response.statusText);
      })
      .catch(function (error) {
        console.log(error);
      });
    
    console.log(`Sending message to : ${phoneNumber} \n${message}`);
}

async function getDryRunStatus() {
  const {data, error } = await supabase.from("sms_schedule").select("*").eq("id", 3);

  if (error) {
    console.log(error);
    return;
  }

  return data[0].test;
}

module.exports = { getAttendance, sendSMS, executeSMS, getDryRunStatus };