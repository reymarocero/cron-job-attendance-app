const cron = require("node-cron");
const dbJob = require("./src/dbjob");
const express = require("express");
const app = express();

// 17 = 5PM
// Monday-Friday = recurring schedule

let _test = false;

// cron.schedule("*/10 * * * * *", async () => {
//     const status = await dbJob.getDryRunStatus();

//     if (status === 0) {
//         console.log("Not running...");
//         return;
//     }

//     await dbJob.executeSMS();
//     console.log("Running testing...");
// });

// cron.schedule("10 * * * *", async () => {
//     // await dbJob.executeSMS();
//     const status = getDryStatus();

//     if (status === true) {
//         dbJob.executeSMS();
//     }

// });
app.get("/", (req, res) => {
    res.send("Running...");
});

app.listen(3000)
